﻿# Получение событий из журнала приложений, отфильтрованных по провайдерам Chrome, chromoting или Microsoft-Windows-RemoteDesktopServices-RdpCoreTS
$RDPAuths = Get-WinEvent -LogName 'Application' -FilterXPath @'
<QueryList>
  <Query Id="0" Path="Application">
    <Select Path="Application">*[System[Provider[@Name='Chrome' or @Name='chromoting' or @Name='Microsoft-Windows-RemoteDesktopServices-RdpCoreTS']]]</Select>
  </Query>
</QueryList>
'@

# Словарь для соответствия индексов операций их строковым описаниям
$OpIndex = @{
        1   = "connected"
        2 = "disconnected"
        3   = "access denied"
        4 = "set IP"
        5 = "host up"
}

# Преобразование каждого события в формат XML для удобства извлечения данных
[xml[]]$xml=$RDPAuths|Foreach{$_.ToXml()}
$EventData = Foreach ($event in $xml.Event) { 
    $evId=[int]$event.System.EventID.'#text'

    # Создание пользовательского объекта для данных событий
    
    # Если идентификатор события равен 4 (установка IP), извлекаются специфические данные
    if($evId -eq 4){
        New-Object PSObject -Property @{  
            TimeCreated = (Get-Date ($event.System.TimeCreated.SystemTime) -Format 'yyyy-MM-dd hh:mm:ss')
            Source   = $event.System.Provider.Name
            Target = $event.System.Computer
        
            ClientIP = $event.EventData.Data[1]
            Type = $event.EventData.Data[4]
        
            Operation=$OpIndex[$evId ]
        }
    }

    # Для других идентификаторов событий используется другая структура данных
    if($evId -ne 4){
    
        New-Object PSObject -Property @{  
            TimeCreated = (Get-Date ($event.System.TimeCreated.SystemTime) -Format 'yyyy-MM-dd hh:mm:ss')
            Source   = $event.System.Provider.Name
            Target = $event.System.Computer
        
            ClientIP = "-"
            Type = "-"    
            Operation=$OpIndex[$evId ]
        }
    }
}

# Вывод данных в форматированную таблицу
$EventData | FT TimeCreated,Operation,ClientIP,Target,Type,Source
